package engine

import (
	"testing"

	"bitbucket.org/clayts/canvas"
	"bitbucket.org/clayts/geometry"
	"bitbucket.org/clayts/gla"
)

func TestEngine(t *testing.T) {
	e := NewEngine()
	b := e.Space().NewBody()
	tex := gla.NewTextureFromFile("rainbowtall.png", true)
	s := b.
		NewShape(
			geometry.NewCircle(0, 0, 1, 0),
		)
	s.
		SetRenderFunction(func(self *Shape, cvs *canvas.Canvas) {
			cvs.LightAll()
			cvs.Paint(tex, geometry.IdentityTransform, self.Body().Transform(), self.Geometry(), 0)
		}).
		// SetGraphicsFile(
		// 	"rainbowtall.png").
		// SetGraphicsSmooth(true).
		// SetGraphicsWrap(gla.Mirror).
		// SetGraphicsSrc(geometry.IdentityTransform).
		SetDensity(20)
	e.Space().SetGravity(geometry.NewVector(0, -1))
	// gla.CreateWindow("Engine Text", geometry.NewVector(800, 800), func(win *gla.Window) bool {
	// 	e.Update()
	// 	// e.Render()
	// 	return true
	// })
	e.CreateWindow("Engine Test", geometry.Vector{800, 800})
}
