package engine

import (
	"fmt"
	"math"
	"os"

	"azul3d.org/engine/native/cp"
	"bitbucket.org/clayts/canvas"
	"bitbucket.org/clayts/geometry"
)

type Shape struct {
	physics  []*cp.Shape
	body     *Body
	geometry geometry.Shape
	// graphics struct {
	// 	file   string
	// 	smooth bool
	// 	wrap   gla.TextureOverflowOperation
	// 	src    geometry.Transform
	// 	depth  float32
	// }
	updateF   func(self *Shape)
	collideF  func(Collision) bool
	contactF  func(Collision) bool
	separateF func(Collision)
	renderF   func(self *Shape, cvs *canvas.Canvas)
	contents  interface{}
}

func (spc *Space) NewEphemeralShape() *Shape {
	b := &Body{}
	b.space = spc
	s := &Shape{}
	s.body = b
	spc.ephemeral[s] = struct{}{}
	return s
}

func (b *Body) NewShape(sh geometry.Shape) *Shape {
	s := &Shape{}
	s.body = b
	s.geometry = sh
	spc := b.Space()
	s.physics = physicsShapes(spc.slop, b.physics, sh)
	// var fixRot bool
	// if b.physics.Moment() == math.Inf(1) {
	// 	fixRot = true
	// }
	spc.safely(func() {
		for _, p := range s.physics {
			p.SetUserData(s)
			p.SetSensor(true)
			p.SetBody(b.physics)
			spc.physics.AddShape(p)
		}
	})
	// if fixRot {
	// 	b.physics.SetMoment(math.Inf(1))
	// }
	return s
}
func (s *Shape) Contents() interface{} { return s.contents }

func (s *Shape) SetContents(i interface{}) *Shape {
	s.contents = i
	return s
}

//return false to make physics ignore this collision
func (r *Shape) SetCollideFunction(f func(Collision) bool) *Shape {
	r.collideF = f
	if r.contactF == nil && r.separateF == nil {
		if f == nil {
			r.setCollisionType(nonCollider)
		} else {
			r.setCollisionType(collider)
		}
	}
	return r
}

//return false to make physics ignore this collision
func (r *Shape) SetContactFunction(f func(Collision) bool) *Shape {
	r.contactF = f
	if r.collideF == nil && r.separateF == nil {
		if f == nil {
			r.setCollisionType(nonCollider)
		} else {
			r.setCollisionType(collider)
		}
	}
	return r
}

//return false to make physics ignore this collision
func (r *Shape) SetSeparateFunction(f func(Collision)) *Shape {
	r.separateF = f
	if r.collideF == nil && r.contactF == nil {
		if f == nil {
			r.setCollisionType(nonCollider)
		} else {
			r.setCollisionType(collider)
		}
	}
	return r
}

func (r *Shape) setCollisionType(c cp.CollisionType) *Shape {
	for _, p := range r.physics {
		p.SetCollisionType(c)
	}
	return r
}

func (s *Shape) collide(c Collision) bool {
	if s.collideF != nil {
		return s.collideF(c)
	}
	return true
}

func (s *Shape) contact(c Collision) bool {
	if s.contactF != nil {
		return s.contactF(c)
	}
	return true
}

func (s *Shape) separate(c Collision) {
	if s.separateF != nil {
		s.separateF(c)
	}
}

func (s *Shape) SetUpdateFunction(f func(self *Shape)) *Shape {
	s.updateF = f
	if f != nil {
		s.Space().updaters[s] = struct{}{}
	} else {
		delete(s.Space().updaters, s)
	}
	return s
}

func (s *Shape) Geometry() geometry.Shape {
	return s.geometry
}

func (s *Shape) SetDensity(v float32) *Shape {
	var fixRot bool
	b := s.Body()
	if b.physics.Moment() == math.Inf(1) {
		fixRot = true
	}
	for _, p := range s.physics {
		p.SetDensity(float64(v))
	}
	if fixRot {
		b.physics.SetMoment(math.Inf(1))
	}
	return s
}

func (s *Shape) EachCollision(f func(c Collision)) *Shape {
	oMap := make(map[*Shape]struct{})
	fmt.Println("each collision")
	s.Body().physics.EachArbiter(func(b *cp.Body, a *cp.Arbiter) {
		os.Exit(0)
		fmt.Println("arb")
		self, other := a.Shapes()
		c := newCollision(a)
		oD := other.UserData().(*Shape)
		if _, ok := oMap[oD]; ok {
			fmt.Println("already processed other")
			return
		}
		oMap[oD] = struct{}{}
		if self.UserData().(*Shape) == s {
			fmt.Println("running func")
			f(c)
		} else {
			fmt.Println("self is not this shape")
		}
	})
	return s
}

func (s *Shape) Remove() {
	spc := s.Space()
	if s.physics == nil {
		delete(spc.ephemeral, s)
		delete(spc.updaters, s)
		return
	}
	spc.safely(func() {
		delete(spc.updaters, s)
		for _, p := range s.physics {
			spc.physics.RemoveShape(p)
		}
	})
}

func (s *Shape) CollisionCount() int {
	n := 0
	s.EachCollision(func(c Collision) {
		n++
	})
	return n
}

func (s *Shape) SetFriction(v float32) *Shape {
	for _, p := range s.physics {
		p.SetFriction(float64(v))
	}
	return s
}

func (s *Shape) SetSurfaceVelocity(v geometry.Vector) *Shape {
	for _, p := range s.physics {
		p.SetSurfaceVelocity(e2pVector(v))
	}
	return s
}

func (s *Shape) Solid() bool {
	return !s.physics[0].Sensor()
}

func (s *Shape) SetSolid(v bool) *Shape {
	for _, p := range s.physics {
		p.SetSensor(!v)
	}
	return s
}

func (s *Shape) SetRenderFunction(f func(self *Shape, cvs *canvas.Canvas)) *Shape {
	s.renderF = f
	return s
}

// func (s *Shape) SetGraphics(file string, smooth bool, wrap gla.TextureOverflowOperation, src geometry.Transform, depth float32) *Shape {
// 	// p := canvas.Canvas.Paint(file, smooth, wrap, src, dst, sh, depth)
// 	// s.graphics = &canvas.Painting{File: file, Smooth: smooth, Wrap: wrap, Src: src, Dst: geometry.Transform{}, Sh: s.Geometry(), Depth: depth}
// 	s.graphics.file = file
// 	s.graphics.smooth = smooth
// 	s.graphics.wrap = wrap
// 	s.graphics.src = src
// 	s.graphics.depth = depth
// 	return s
// }

// func (s *Shape) NewABBTo() *Shape {
// 	// p := canvas.Canvas.Paint(file, smooth, wrap, src, dst, sh, depth)
// 	// s.graphics = &canvas.Painting{File: file, Smooth: smooth, Wrap: wrap, Src: src, Dst: geometry.Transform{}, Sh: s.Geometry(), Depth: depth}
// 	s.graphics.src = geometry.NewAABToAABTransform(s.geometry.BoundingBox(), canvas.ScreenAAB)
// 	return s
// }

//
// func (s *Shape) SetGraphicsSrcFromFileRegion(region geometry.AAB) *Shape {
// 	// p := canvas.Canvas.Paint(file, smooth, wrap, src, dst, sh, depth)
// 	// s.graphics = &canvas.Painting{File: file, Smooth: smooth, Wrap: wrap, Src: src, Dst: geometry.Transform{}, Sh: s.Geometry(), Depth: depth}
// 	s.graphics.src = geometry.NewAABToAABTransform(s.geometry.BoundingBox(), region)
// 	return s
// }
//

func NewAABToFilePixelRegionTransform(file string, region geometry.AAB, dst geometry.AAB) geometry.Transform {
	// s.graphics.file = file
	// p := canvas.Canvas.Paint(file, smooth, wrap, src, dst, sh, depth)
	// s.graphics = &canvas.Painting{File: file, Smooth: smooth, Wrap: wrap, Src: src, Dst: geometry.Transform{}, Sh: s.Geometry(), Depth: depth}
	return geometry.NewAABToAABTransform(dst, region).FollowedBy(canvas.GetImagePixelTransform(file))
	// return s
}

//
// func (s *Shape) SetGraphicsFromSheetRegion(file string, blockSizePx int, region geometry.AAB) *Shape {
// 	// p := canvas.Canvas.Paint(file, smooth, wrap, src, dst, sh, depth)
// 	// s.graphics = &canvas.Painting{File: file, Smooth: smooth, Wrap: wrap, Src: src, Dst: geometry.Transform{}, Sh: s.Geometry(), Depth: depth}
// 	s.graphics.file = file
// 	region = geometry.NewAABFromLBRT(
// 		region.L()*float32(blockSizePx),
// 		region.B()*float32(blockSizePx),
// 		region.R()*float32(blockSizePx),
// 		region.T()*float32(blockSizePx),
// 	)
// 	s.graphics.src = geometry.NewAABToAABTransform(s.geometry.BoundingBox(), region).FollowedBy(canvas.GetImagePixelTransform(file))
// 	return s
// }

func NewAABToSheetRegionTransform(file string, blockSizePx int, region geometry.AAB, dst geometry.AAB) geometry.Transform {
	region = geometry.NewAABFromLBRT(
		region.L()*float32(blockSizePx),
		region.B()*float32(blockSizePx),
		region.R()*float32(blockSizePx),
		region.T()*float32(blockSizePx),
	)
	return geometry.NewAABToAABTransform(dst, region).FollowedBy(canvas.GetImagePixelTransform(file))
}

//
// func (s *Shape) SetGraphicsSrcFromFilePixelRegion(file string, region geometry.AAB, smooth bool, wrap gla.TextureOverflowOperation, depth float32) *Shape {
// 	// p := canvas.Canvas.Paint(file, smooth, wrap, src, dst, sh, depth)
// 	// s.graphics = &canvas.Painting{File: file, Smooth: smooth, Wrap: wrap, Src: src, Dst: geometry.Transform{}, Sh: s.Geometry(), Depth: depth}
// 	s.graphics.file = file
// 	s.graphics.smooth = smooth
// 	s.graphics.wrap = wrap
// 	s.graphics.src = geometry.NewAABToAABTransform(s.geometry.BoundingBox(), region).FollowedBy(canvas.GetImagePixelTransform(file))
// 	s.graphics.depth = depth
// 	return s
// }

// func (s *Shape) SetGraphicsFile(file string) *Shape {
// 	s.graphics.file = file
// 	return s
// }
//
// func (s *Shape) SetGraphicsSmooth(smooth bool) *Shape {
// 	s.graphics.smooth = smooth
// 	return s
// }
//
// func (s *Shape) SetGraphicsWrap(wrap gla.TextureOverflowOperation) *Shape {
// 	s.graphics.wrap = wrap
// 	return s
// }
//
// func (s *Shape) SetGraphicsSrc(src geometry.Transform) *Shape {
// 	s.graphics.src = src
// 	return s
// }

//
// func (s *Shape) ApplyGraphicsSrcTransform(src geometry.Transform) *Shape {
// 	s.graphics.src = s.graphics.src.After(src)
// 	return s
// }
//
// func (s *Shape) SetGraphicsDepth(depth float32) *Shape {
// 	s.graphics.depth = depth
// 	return s
// }

func (s *Shape) Body() *Body {
	return s.body
}

func (s *Shape) Space() *Space {
	return s.body.space
}

//
// func (s *Shape) Transform() geometry.Transform {
// 	return p2eTransform(s.Body().physics)
// }

// func (s *Shape) Apply(cvs *canvas.Canvas) {
// 	if s.graphics.file != "" {
// 		cvs.Paint(s.graphics.file, s.graphics.smooth, s.graphics.wrap, s.graphics.src, s.Transform(), s.geometry, s.graphics.depth)
// 	}
// }
