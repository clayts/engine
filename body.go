package engine

import (
	"math"

	"azul3d.org/engine/native/cp"
	"bitbucket.org/clayts/geometry"
)

type Body struct {
	space   *Space
	physics *cp.Body
}

func (s *Space) NewStaticBody() *Body {
	b := &Body{}
	b.space = s
	b.physics = cp.BodyNewStatic()
	b.physics.SetUserData(b)
	s.safely(func() {
		s.physics.AddBody(b.physics)
	})
	return b
}

func (s *Space) NewKinematicBody() *Body {
	b := &Body{}
	b.space = s
	b.physics = cp.BodyNewKinematic()
	b.physics.SetUserData(b)
	s.safely(func() {
		s.physics.AddBody(b.physics)
	})
	return b
}

func (s *Space) NewBody() *Body {
	b := &Body{}
	b.space = s
	b.physics = cp.BodyNew(0, 0)
	b.physics.SetUserData(b)
	s.safely(func() {
		s.physics.AddBody(b.physics)
	})
	return b
}

func (s *Space) NewFixedRotationBody() *Body {
	b := &Body{}
	b.space = s
	b.physics = cp.BodyNew(0, math.Inf(1))
	b.physics.SetUserData(b)
	s.safely(func() {
		s.physics.AddBody(b.physics)
	})
	return b
}

func (b *Body) Space() *Space {
	return b.space
}

func (b *Body) ApplyForce(v geometry.Vector) *Body {
	b.physics.ApplyForceAtLocalPoint(e2pVector(v), cp.Vect{X: 0, Y: 0})
	return b
}

func (b *Body) ApplyImpulse(v geometry.Vector) *Body {
	b.physics.ApplyImpulseAtLocalPoint(e2pVector(v), cp.Vect{X: 0, Y: 0})
	return b
}

func (b *Body) SetPosition(v geometry.Vector) *Body {
	b.physics.SetPosition(e2pVector(v))
	return b
}

func (b *Body) SetVelocity(v geometry.Vector) *Body {
	b.physics.SetVelocity(e2pVector(v))
	return b
}

func (b *Body) SetRotation(v float32) *Body {
	b.physics.SetAngle(float64(v))
	return b
}

func (b *Body) Position() geometry.Vector {
	return p2eVector(b.physics.Position())
}

func (b *Body) Mass() float32 {
	return float32(b.physics.Mass())
}

func (b *Body) Velocity() geometry.Vector {
	return p2eVector(b.physics.Velocity())
}

func (b *Body) Remove() {
	spc := b.Space()
	spc.safely(func() {
		b.EachShape(func(s *Shape) bool {
			s.Remove()
			return true
		})
		spc.physics.RemoveBody(b.physics)
	})
}

func (b *Body) EachShape(f func(s *Shape) bool) *Body {
	tab := make(map[*Shape]struct{})
	b.physics.EachShape(func(b *cp.Body, s *cp.Shape) {
		tab[s.UserData().(*Shape)] = struct{}{}
	})
	for s := range tab {
		if !f(s) {
			break
		}
	}
	return b
}

func (b *Body) Transform() geometry.Transform {
	return p2eTransform(b.physics)
}

func (b *Body) ShapeCount() int {
	tab := make(map[*Shape]struct{})
	b.physics.EachShape(func(b *cp.Body, s *cp.Shape) {
		tab[s.UserData().(*Shape)] = struct{}{}
	})
	return len(tab)
}
