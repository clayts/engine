package engine

import (
	"sync"

	"bitbucket.org/clayts/canvas"
	"bitbucket.org/clayts/geometry"
	"bitbucket.org/clayts/gla"
)

type Engine struct {
	space    *Space
	graphics struct {
		prev    *sync.WaitGroup
		canvas1 *canvas.Canvas
		canvas2 *canvas.Canvas
		camera  geometry.Transform
		target  geometry.AAB
		window  *gla.Window
	} // inputs *Inputs
}

func NewEngine() *Engine {
	e := &Engine{}
	e.space = newSpace()
	e.graphics.camera = geometry.IdentityTransform
	e.graphics.target = canvas.ScreenAAB
	e.graphics.canvas1 = canvas.NewCanvas()
	e.graphics.canvas2 = canvas.NewCanvas()
	// e.graphics.canvas.SetLighting(true)
	// e.inputs = NewInputs()
	return e
}

func (e *Engine) CreateWindow(title string, size geometry.Vector) {
	gla.CreateWindow(title, size, func(w *gla.Window) bool {
		e.graphics.window = w
		e.update()
		return true
	})
}

func (e *Engine) Space() *Space {
	return e.space
}

//
// func (e *Engine) Canvas() *canvas.Canvas {
// 	return e.graphics.canvas
// }

// func (e *Engine) render() {
//
// 	e.graphics.prev.Add(1)
// 	go func() {
// 		e.graphics.prev.Done()
// 	}()
// }

func (e *Engine) update() {
	wg := &sync.WaitGroup{}
	wg.Add(1)
	//TODO test if this actually helps with high physics and graphics load
	go func() {
		e.Space().Update()
		e.Space().Render(e.graphics.camera, e.graphics.canvas1)
		wg.Done()
	}()
	e.graphics.canvas2.Render(e.graphics.camera, e.graphics.target, e.Window())
	e.graphics.canvas2.Clear()
	wg.Wait()
	e.graphics.canvas1, e.graphics.canvas2 = e.graphics.canvas2, e.graphics.canvas1
}

func (e *Engine) Window() *gla.Window {
	return e.graphics.window
}

func (e *Engine) Camera() geometry.Transform {
	return e.graphics.camera
}

func (e *Engine) SetCamera(c geometry.Transform) {
	e.graphics.camera = c
}
