package engine

import (
	"fmt"

	"azul3d.org/engine/native/cp"
	"bitbucket.org/clayts/geometry"
)

func p2eTransform(b *cp.Body) geometry.Transform {
	tsl := b.Position()
	rot := b.Rotation()
	c, s := rot.X, rot.Y
	return geometry.NewTransformFromSinCosTranslation(float32(s), float32(c), p2eVector(tsl))
}

func p2eVector(v cp.Vect) geometry.Vector {
	return geometry.Vector{float32(v.X), float32(v.Y)}
}

func e2pVector(v geometry.Vector) cp.Vect {
	return cp.Vect{X: float64(v.X()), Y: float64(v.Y())}
}

func e2pAAB(v geometry.AAB) cp.BB {
	a := v.BoundingBox()
	return cp.BBNew(float64(a.L()), float64(a.B()), float64(a.R()), float64(a.T()))
}

func physicsShapes(slop float32, body *cp.Body, sh geometry.Shape) []*cp.Shape {
	var answer []*cp.Shape
	switch sh.ShapeType() {
	case geometry.LineType:
		fvi := sh.FirstVertexIndex()
		l1 := sh.Vertex(fvi)
		l2 := sh.Vertex(sh.NextVertexIndexClockwise(fvi))
		shape := body.SegmentShapeNew(e2pVector(l1), e2pVector(l2), 0)
		answer = append(answer, shape)
	case geometry.PointType:
		panic("cannot create Shape of this ShapeType")
	case geometry.AABType:
		rad := slop
		box := sh.BoundingBox()
		mw := box.RadiusX()
		if my := box.RadiusY(); my < mw {
			mw = my
		}
		if mw < rad {
			rad = mw
		}
		answer = append(
			answer,
			body.BoxShapeNew2(
				e2pAAB(
					geometry.NewAABFromCentreRadius(
						box.Centre(),
						box.Radius().MinusXY(rad),
					),
				),
				float64(rad),
			),
		)
	case geometry.ConvexType:
		box := sh.BoundingBox()
		cnt := box.Centre()
		var vertices []cp.Vect
		i := sh.FirstVertexIndex()
		start := i
		rad := slop
		ax := geometry.AxisOfMinimumWidth(sh)
		mw := ax.Length() / 2
		if mw < rad {
			rad = mw
		}
		for {
			v := sh.Vertex(i)
			if v.X() > cnt.X() {
				v = v.MinusX(rad)
			} else {
				v = v.PlusX(rad)
			}
			if v.Y() > cnt.Y() {
				v = v.MinusY(rad)
			} else {
				v = v.PlusY(rad)
			}

			vertices = append(vertices, e2pVector(v))
			i = sh.NextVertexIndexAnticlockwise(i)
			if len(vertices) == 8 || i == start {
				shape := body.PolyShapeNew(vertices, cp.TransformIdentity, float64(rad))
				answer = append(answer, shape)
				if i == start {
					break
				}
				vertices = append(vertices[:1], vertices[len(vertices)-1:]...)
			}
		}
	case geometry.ConcaveType:
		box := sh.BoundingBox()
		cnt := box.Centre()
		cci := sh.FirstConvexComponentIndex()
		ccistart := cci
		rad := slop
		ax := geometry.AxisOfMinimumWidth(sh)
		mw := ax.Length()
		if mw/2 < rad {
			rad = mw / 2
		}
		for {
			cv := sh.ConvexComponent(cci)
			var vertices []cp.Vect
			i := cv.FirstVertexIndex()
			start := i
			for {
				v := cv.Vertex(i)
				if v.X() > cnt.X() {
					v = v.MinusX(rad)
				} else {
					v = v.PlusX(rad)
				}
				if v.Y() > cnt.Y() {
					v = v.MinusY(rad)
				} else {
					v = v.PlusY(rad)
				}
				vertices = append(vertices, e2pVector(v))
				i = cv.NextVertexIndexAnticlockwise(i)
				if len(vertices) == 8 || i == start {
					shape := body.PolyShapeNew(vertices, cp.TransformIdentity, float64(rad))
					answer = append(answer, shape)
					if i == start {
						break
					}
					vertices = append(vertices[:1], vertices[len(vertices)-1:]...)
				}
			}
			cci = sh.NextConvexComponentIndex(cci)
			if cci == ccistart {
				break
			}
		}
	case geometry.CircleType:
		box := sh.BoundingBox()
		shape := body.CircleShapeNew(float64(box.RadiusX()), e2pVector(box.Centre()))
		answer = append(answer, shape)
	}
	for _, a := range answer {
		fmt.Println(a)
	}
	return answer
}
