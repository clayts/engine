package engine

import (
	"azul3d.org/engine/native/cp"
	"bitbucket.org/clayts/geometry"
)

type Collision struct {
	physics *cp.Arbiter
	reverse bool
}

func newCollision(physics *cp.Arbiter) Collision {
	return Collision{physics, false}
}

func (c Collision) Reversed() Collision {
	return Collision{c.physics, true}
}

func (c Collision) Shapes() (*Shape, *Shape) {
	a, b := c.physics.Shapes()
	if c.reverse {
		a, b = b, a
	}
	return a.UserData().(*Shape), b.UserData().(*Shape)
}

func (c Collision) Normal() geometry.Vector {
	v := p2eVector(c.physics.GetNormal())
	if c.reverse {
		return v.Opposite()
	}
	return v
}

func (c Collision) Friction() float32 {
	return float32(c.physics.Friction())
}

func (c Collision) SetFriction(f float32) {
	c.physics.SetFriction(float64(f))
}
