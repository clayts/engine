package engine

import (
	"azul3d.org/engine/native/cp"
	"bitbucket.org/clayts/canvas"
	"bitbucket.org/clayts/geometry"
)

type Space struct {
	engine *Engine
	// substitute *cp.Body
	physics *cp.Space
	slop    float32
	speed   float64
	// graphics *canvas.Canvas
	ephemeral  map[*Shape]struct{}
	updaters   map[*Shape]struct{}
	postStepFs []func()
}

const (
	collider cp.CollisionType = iota
	nonCollider
)

// func (spc *Space) Canvas() *canvas.Canvas {
// 	return spc.graphics
// }

func newSpace() *Space {
	s := &Space{}
	// s.upd = &updater{}
	// s.upd.maxProcs = runtime.NumCPU()
	s.updaters = make(map[*Shape]struct{})
	// s.updaters = make(map[*Shape]struct{})
	s.ephemeral = make(map[*Shape]struct{})
	// s.graphics = canvas.NewCanvas()
	s.physics = cp.SpaceNew()
	s.physics.SetUserData(s)
	s.speed = float64(1) / float64(60)
	s.slop = 0.01
	handlerCtoC := &cp.CollisionHandler{}
	handlerCtoC.TypeA = collider
	handlerCtoC.TypeB = collider
	handlerCtoC.BeginFunc = func(arb *cp.Arbiter, space *cp.Space, userData interface{}) bool {
		a, b := arb.Shapes()
		ar := a.UserData().(*Shape)
		br := b.UserData().(*Shape)
		col := newCollision(arb)
		return ar.collide(col) && br.collide(col.Reversed())
	}
	handlerCtoC.PreSolveFunc = func(arb *cp.Arbiter, space *cp.Space, userData interface{}) bool {
		a, b := arb.Shapes()
		ar := a.UserData().(*Shape)
		br := b.UserData().(*Shape)
		col := newCollision(arb)
		return ar.contact(col) && br.contact(col.Reversed())
	}
	handlerCtoC.SeparateFunc = func(arb *cp.Arbiter, space *cp.Space, userData interface{}) {
		a, b := arb.Shapes()
		ar := a.UserData().(*Shape)
		br := b.UserData().(*Shape)
		col := newCollision(arb)
		ar.separate(col)
		br.separate(col.Reversed())
	}

	handlerCtoN := &cp.CollisionHandler{}
	handlerCtoN.TypeA = collider
	handlerCtoN.TypeB = nonCollider
	handlerCtoN.BeginFunc = func(arb *cp.Arbiter, space *cp.Space, userData interface{}) bool {
		a, _ := arb.Shapes()
		ar := a.UserData().(*Shape)
		col := newCollision(arb)
		return ar.collide(col)
	}
	handlerCtoN.PreSolveFunc = func(arb *cp.Arbiter, space *cp.Space, userData interface{}) bool {
		a, _ := arb.Shapes()
		ar := a.UserData().(*Shape)
		col := newCollision(arb)
		return ar.contact(col)
	}
	handlerCtoN.SeparateFunc = func(arb *cp.Arbiter, space *cp.Space, userData interface{}) {
		a, _ := arb.Shapes()
		ar := a.UserData().(*Shape)
		col := newCollision(arb)
		ar.separate(col)
	}

	s.physics.AddCollisionHandler(collider, collider, handlerCtoC)
	s.physics.AddCollisionHandler(collider, nonCollider, handlerCtoN)
	// s.substitute = cp.BodyNewStatic()
	return s
}

func (s *Space) safely(f func()) {
	if s.physics.IsLocked() {
		s.postStepFs = append(s.postStepFs, f)
	} else {
		f()
	}
}

// func (s *Space) StartPhysics() {
// 	go func() {
// 		for {
// 		}
// 	}()
// }

func (s *Space) EphemeralCount() int { return len(s.ephemeral) }

func (s *Space) Update() {
	// wg := &sync.WaitGroup{}
	// wg.Add(2)
	// go func() {
	s.physics.Step(s.speed)
	// wg.Done()
	// }()
	// go func() {
	for u := range s.updaters {
		u.updateF(u)
	}
	// wg.Done()
	// }()
	// wg.Wait()
	for _, f := range s.postStepFs {
		f()
	}
	s.postStepFs = s.postStepFs[:0]
}

func (s *Space) SetGravity(v geometry.Vector) {
	s.physics.SetGravity(e2pVector(v))
}

func (e *Space) Render(cam geometry.Transform, cvs *canvas.Canvas) {
	// wg := &sync.WaitGroup{}
	// wg.Add(1)
	// func() {
	// e.physics.Step(e.speed)
	// wg.Done()
	// }()
	e.Query(
		geometry.NewTransformShape(
			canvas.ScreenAAB,
			cam.Inverse(),
		).BoundingBox(),
		func(s *Shape) bool {
			if s.renderF != nil {
				s.renderF(s, cvs)
			}
			return true
		},
	)
	for eph := range e.ephemeral {
		if eph.renderF != nil {
			eph.renderF(eph, cvs)
		}
	}
	// cvs.Render(cam, dst)
	// cvs.Clear()
	// wg.Wait()
}

func (e *Space) QueryLineFirst(l geometry.Line) *Shape {
	ps, _ := e.physics.SegmentQueryFirst(e2pVector(l.Start()), e2pVector(l.End()), 0, cp.SHAPE_FILTER_ALL)
	if ps == nil {
		return nil
	}
	return ps.UserData().(*Shape)
}

func (e *Space) Query(sh geometry.Shape, f func(n *Shape) bool) {
	shapes := make(map[*Shape]struct{})
	switch sh.ShapeType() {
	case geometry.AABType:
		e.physics.BBQuery(e2pAAB(sh.BoundingBox()), cp.SHAPE_FILTER_ALL, func(s *cp.Shape, data interface{}) {
			shapes[s.UserData().(*Shape)] = struct{}{}
		}, nil)
	case geometry.CircleType:
		phs := physicsShapes(e.slop, e.NewStaticBody().physics, sh)[0]
		e.physics.ShapeQuery(phs, func(s *cp.Shape, points *cp.ContactPointSet, data interface{}) {
			shapes[s.UserData().(*Shape)] = struct{}{}
		}, nil)
	case geometry.LineType:
		i := sh.FirstConvexComponentIndex()
		a := sh.Vertex(i)
		b := sh.Vertex(sh.NextVertexIndexClockwise(i))
		e.physics.SegmentQuery(e2pVector(a), e2pVector(b), 0, cp.SHAPE_FILTER_ALL, func(s *cp.Shape, point, normal cp.Vect, alpha float64, data interface{}) {
			shapes[s.UserData().(*Shape)] = struct{}{}
		}, nil)
	case geometry.PointType:
		e.physics.PointQuery(e2pVector(sh.Vertex(sh.FirstVertexIndex())), 0, cp.SHAPE_FILTER_ALL, func(s *cp.Shape, point cp.Vect, distance float64, gradient cp.Vect, data interface{}) {
			shapes[s.UserData().(*Shape)] = struct{}{}
		}, nil)
	case geometry.ConvexType, geometry.ConcaveType:
		phs := physicsShapes(e.slop, e.NewStaticBody().physics, sh)
		for _, p := range phs {
			e.physics.ShapeQuery(p, func(s *cp.Shape, points *cp.ContactPointSet, data interface{}) {
				shapes[s.UserData().(*Shape)] = struct{}{}
			}, nil)
		}
	}

	for s := range shapes {
		if !f(s) {
			break
		}
	}
}

func (spc *Space) BodyCount() int {
	tab := make(map[*Body]struct{})
	spc.physics.EachBody(spc.physics, func(b *cp.Body) {
		tab[b.UserData().(*Body)] = struct{}{}
	})
	return len(tab)
}

func (spc *Space) ShapeCount() int {
	tab := make(map[*Shape]struct{})
	spc.physics.EachShape(spc.physics, func(s *cp.Shape) {
		tab[s.UserData().(*Shape)] = struct{}{}
	})
	return len(tab)
}
